# SIO Dashboard

"Develop an web app to provide to a complete information of the company status, regarding sales, purchases, inventory, accounts, etc."

## Tipo de negocio.

Temos de desenvolver um negocio, com pelo menos 1 ano de informação de venda e compra, etc. Para podermos mostrar a informação na web app.

 - *SAFT* vêm do Primavera, este é importado para a web app.

## Dependencies

### Frontend
- [Material UI](https://material-ui.com/)
- [React Router](https://www.sitepoint.com/react-router-complete-guide/)
- [Notistack](https://github.com/iamhosseindhv/notistack)

### Backend
- [Multer](https://www.npmjs.com/package/multer)
- [Node-postgresql](https://node-postgres.com/)
- [xml2js](https://www.npmjs.com/package/xml2js)

## Tecnologia a usar

- Containers -> [Docker](https://www.docker.com/)
- Database -> [PostgreSQL](https://www.postgresql.org/docs)
- Frontend -> [React](https://reactjs.org/)
  - Frameworks -> [Bootstrap](https://getbootstrap.com/), [Cube](https://real-time-dashboard.cube.dev/)
- Backend -> [Node](https://nodejs.org/en/)
  - Frameworks -> [Expressjs](https://expressjs.com/)

## Running

This project is containerized meaning that it works based on containers for an easy setup.

- `docker-compose build` -> This command will build the docker containers.
- `docker-compose up -d` -> This command will start the docker containers.
- `docker-compose up -d --build` -> This command will build and run the docker containers.
- `docker-compose stop` -> This command will stop all of the containers.
- `docker-compose exec -it <container_name> bash` -> This command will start a shell for the container, where `<container_name>` can be:
  - back-end -> For the backend container;
  - front-end -> For the frontend container;
  - db -> For the database.

### Backend

This can be run locally from the command line:
- `npm install` -> Will install the dependencies.
- `npm start` -> Will start the **API**.

The default **URL** is [localhost:8080](http://localhost:8080) and the routes always have to start with `/api/`, this is done automatically.
See `routes/test.js` for an exemple, on how to make routes.

### Frontend

This can be run locally from the command line:
- `npm install` -> Will install the dependencies.
- `npm start` -> Will start the Web app.

The default **URL** is [http://localhost:3000](http://localhost:3000).

### Database

This can be run locally by installing [PostgreSQL](https://www.postgresql.org/docs), there is also a helper script `create_db.sh` that executes the *sql* files. Also run on docker for easy of use.
To update the database you can run the following command `./docker_db.sh`.

# Projetos exemplos

- [PostgreSQL and python with docker](https://gitlab.com/Zyr0/asi-python)
- [Dashboard Images](https://duckduckgo.com/?q=dashboards+exemplos&t=ffab&iar=images&iax=images&ia=images)
- [Docker with react](https://mherman.org/blog/dockerizing-a-react-app/)

#### OTHER LINKS
- [cheatsheet react typescript](https://react-typescript-cheatsheet.netlify.app/docs/basic/setup)
- [typescript cheatsheets](https://github.com/typescript-cheatsheets/react)
- [typescript react starter](https://github.com/microsoft/TypeScript-React-Starter)
- [more typescript cheatsheet](https://github.com/typescript-cheatsheets/react#reacttypescript-cheatsheets)
- [react typescript patterns](https://levelup.gitconnected.com/ultimate-react-component-patterns-with-typescript-2-8-82990c516935)
- [basic use of typescript and react](https://egghead.io/courses/use-typescript-to-develop-react-applications)
- [simple react router typescript](https://www.pluralsight.com/guides/react-router-typescript)
- [route with props typescript](https://medium.com/@robjtede/the-real-routewithprops-react-component-in-typescript-defacde01991)
- We are not using redux, but might come in handy.
  [react redux typescript gide](https://github.com/piotrwitek/react-redux-typescript-guide#react--redux-in-typescript---static-typing-guide)
