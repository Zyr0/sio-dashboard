#!/bin/sh
user=''
db=''
dir='.'

set -e

show_usage() {
  printf "Usage: $0 [option] username database\n"
  printf "\n"
  printf "Options:\n"
  printf "  -i|--init. Run db.sql file\n"
  printf "  -t|--table. Run table.sql file\n"
  printf "  -f|--func. Run functions files\n"
  printf "  -v|--views. Run views files\n"
  printf "  -p|--proc. Run procedures files\n"
  printf "  -a|-all. Run table, functions and views\n"
  printf "  -h|--help. Print help\n\n"
}

get_user() {
  read -p "Username: " user
}

get_db() {
  read -p "Database: " db
}

create_db() {
  [ -z $user ] && get_user "fd"
  echo "Running db.sql ..."
  psql -U $user -f $dir/db.sql
}

run_tables() {
  [ -z $user ] && get_user
  [ -z $db ] && get_db
  echo "Running tables.sql ..."
  psql -U $user -f $dir/tables.sql -d $db
}

run_functions() {
  [ -z $user ] && get_user
  [ -z $db ] && get_db
  if [ "$(ls -A $dir/functions/)" ]; then
    for file in $dir/functions/*
    do
      echo 'Running file' $file '...'
      psql -U $user -f $file -d $db
    done
  fi
}

run_procedures() {
  [ -z $user ] && get_user
  [ -z $db ] && get_db
  if [ "$(ls -A $dir/procedures/)" ]; then
    for file in $dir/procedures/*
    do
      echo 'Running file' $file '...'
      psql -U $user -f $file -d $db
    done
  fi
}

run_views() {
  [ -z $user ] && get_user
  [ -z $db ] && get_db
  if [ "$(ls -A $dir/views/)" ]; then
    for file in $dir/views/*
    do
      echo 'Running file' $file '...'
      psql -U $user -f $file -d $db
    done
  fi
}

run_all() {
  run_tables
  run_functions
  run_views
  run_procedures
}

if [ $1 != '-d' ]; then
  [ -z $2 ] && show_usage && exit || user=$2
  [ -z $3 ] && [ $1 != '-i' ] && show_usage && exit || db=$3
fi

case "$1" in
  -d)
    [ $PWD != '/app' ] && dir='/app'
    user='sio'
    db='tp'
    run_all
    ;;
  -i|--init)
    create_db
    ;;
  -a|--all)
    run_all
    ;;
  -t|--table)
    run_tables
    ;;
  -f|--func)
    run_functions
    ;;
  -v|--views)
    run_views
    ;;
  -p|--proc)
    run_procedures
    ;;
  *)
    show_usage
    ;;
esac
