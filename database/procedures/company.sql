CREATE OR REPLACE PROCEDURE delete_company_by_saft(
  _saft_id INT
)
LANGUAGE plpgsql
AS $$
DECLARE
  _id INT;
  _address_id INT;
BEGIN
  SELECT id INTO _id FROM company WHERE saft_id = _saft_id;
  SELECT address_id INTO _address_id FROM company WHERE saft_id = _saft_id;

  DELETE FROM transaction_journal tj
    USING transaction t, journal j
  WHERE j.company_id = _id AND tj.journal_id = j.id AND tj.transaction_id = t.id;

  DELETE FROM transaction_credit_debit_line tl
    USING transaction t, credit_debit_line l
  WHERE l.company_id = _id AND tl.credit_debit_line_id = l.id AND tl.transaction_id = t.id;

  DELETE FROM credit_debit_line l
    USING account a
  WHERE l.company_id = _id AND l.account_id = a.id;

  DELETE FROM invoice_line il
    USING invoice i, line l
  WHERE i.company_id = _id AND il.invoice_id = i.id AND il.line_id = l.id;

  DELETE FROM line WHERE company_id = _id;
  DELETE FROM invoice WHERE company_id = _id;
  DELETE FROM supplier WHERE company_id = _id;
  DELETE FROM purchases WHERE company_id = _id;
  DELETE FROM product WHERE company_id = _id;
  DELETE FROM customer WHERE company_id = _id;
  DELETE FROM account WHERE company_id = _id;
  DELETE FROM transaction WHERE company_id = _id;
  DELETE FROM journal WHERE company_id = _id;
  DELETE FROM company WHERE saft_id = _saft_id;
  DELETE FROM address WHERE id = _address_id;
  COMMIT;
END; $$;

