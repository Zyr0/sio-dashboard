CREATE TABLE IF NOT EXISTS saft_file (
  id SERIAL PRIMARY KEY,
  name TEXT UNIQUE,
  path_saft TEXT NOT NULL,
  path_purchases TEXT NOT NULL
);

CREATE TYPE files AS (saft TEXT, purchases TEXT);

CREATE TABLE IF NOT EXISTS address (
  id SERIAL PRIMARY KEY,
  street TEXT DEFAULT NULL,
  detail TEXT DEFAULT NULL,
  city TEXT DEFAULT NULL,
  postal_code TEXT DEFAULT NULL,
  country TEXT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS company (
  id SERIAL PRIMARY KEY,
  saft_id INT NOT NULL REFERENCES saft_file(id),
  company_id INT NOT NULL,
  company_name TEXT DEFAULT NULL,
  tax_registration_number INT DEFAULT NULL,
  tax_accounting_basis TEXT DEFAULT NULL,
  tax_entity TEXT DEFAULT NULL,
  fiscal_year INT DEFAULT NULL,
  start_date TEXT DEFAULT NULL,
  end_date TEXT DEFAULT NULL,
  created_date TEXT DEFAULT NULL,
  currency_code VARCHAR(3) DEFAULT NULL,
  product_company_tax_id INT DEFAULT NULL,
  telephone INT DEFAULT NULL,
  fax INT DEFAULT NULL,
  email TEXT DEFAULT NULL,
  address_id INT NOT NULL REFERENCES address(id)
);

CREATE TABLE IF NOT EXISTS account (
  id SERIAL PRIMARY KEY,
  company_id INT NOT NULL REFERENCES company(id),
  account_id TEXT NOT NULL,
  description TEXT DEFAULT NULL,
  g_category TEXT DEFAULT NULL,
  g_code TEXT DEFAULT NULL,
  o_debit FLOAT DEFAULT NULL,
  o_credit FLOAT DEFAULT NULL,
  c_debit FLOAT DEFAULT NULL,
  c_credit FLOAT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS customer (
  id SERIAL PRIMARY KEY,
  company_id INT NOT NULL REFERENCES company(id),
  customer_id TEXT NOT NULL,
  account_id INT REFERENCES account(id) DEFAULT NULL,
  tax_id INT DEFAULT NULL,
  company_name TEXT DEFAULT NULL,
  b_address TEXT DEFAULT NULL,
  city TEXT DEFAULT NULL,
  postal_code TEXT DEFAULT NULL,
  region TEXT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS supplier (
  id SERIAL PRIMARY KEY,
  company_id INT NOT NULL REFERENCES company(id),
  supplier_id TEXT NOT NULL,
  account_id INT REFERENCES account(id) DEFAULT NULL,
  tax_id INT DEFAULT NULL,
  company_name TEXT DEFAULT NULL,
  b_address TEXT DEFAULT NULL,
  city TEXT DEFAULT NULL,
  postal_code TEXT DEFAULT NULL,
  region TEXT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS product (
  id SERIAL PRIMARY KEY,
  company_id INT NOT NULL REFERENCES company(id),
  p_type TEXT NOT NULL,
  p_code TEXT DEFAULT NULL,
  p_group TEXT DEFAULT NULL,
  p_description TEXT DEFAULT NULL,
  p_number_code TEXT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS journal (
  id SERIAL PRIMARY KEY,
  company_id INT NOT NULL REFERENCES company(id),
  journal_id TEXT DEFAULT NULL,
  description TEXT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS credit_debit_line (
  id SERIAL PRIMARY KEY,
  company_id INT NOT NULL REFERENCES company(id),
  record_id TEXT DEFAULT NULL,
  account_id INT NOT NULL REFERENCES account(id),
  source_document TEXT DEFAULT NULL,
  description TEXT DEFAULT NULL,
  amount FLOAT DEFAULT 0,
  l_type TEXT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS transaction (
  id SERIAL PRIMARY KEY,
  company_id INT NOT NULL REFERENCES company(id),
  transaction_id TEXT DEFAULT NULL,
  period TEXT DEFAULT NULL,
  t_date TEXT DEFAULT NULL,
  description TEXT DEFAULT NULL,
  t_type TEXT DEFAULT NULL
);

-- A journal can have more than 1 transaction
CREATE TABLE IF NOT EXISTS transaction_journal (
  id SERIAL PRIMARY KEY,
  journal_id INT NOT NULL REFERENCES journal(id),
  transaction_id INT NOT NULL REFERENCES transaction(id)
);

-- A transaction has more than 1 line
CREATE TABLE IF NOT EXISTS transaction_credit_debit_line (
  id SERIAL PRIMARY KEY,
  credit_debit_line_id INT NOT NULL REFERENCES credit_debit_line(id),
  transaction_id INT NOT NULL REFERENCES transaction(id)
);

CREATE TABLE IF NOT EXISTS invoice (
  id SERIAL PRIMARY KEY,
  company_id INT NOT NULL REFERENCES company(id),
  invoice_no TEXT DEFAULT NULL,
  atcud TEXT DEFAULT NULL,
  status TEXT DEFAULT NULL,
  billing TEXT DEFAULT NULL,
  period TEXT DEFAULT NULL,
  i_type TEXT DEFAULT NULL,
  customer_id INT NOT NULL REFERENCES customer(id),
  transaction_id INT NOT NULL REFERENCES transaction(id),
  tax_payable FLOAT DEFAULT NULL,
  net_total FLOAT DEFAULT NULL,
  gross_total FLOAT DEFAULT NULL,
  withholding_amount FLOAT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS line (
  id SERIAL PRIMARY KEY,
  company_id INT NOT NULL REFERENCES company(id),
  l_number INT DEFAULT NULL,
  product_id INT REFERENCES product(id),
  quantity INT DEFAULT NULL,
  unit TEXT DEFAULT NULL,
  unit_price FLOAT DEFAULT NULL,
  credit_amount FLOAT DEFAULT NULL,
  tax_percentage INT DEFAULT NULL,
  settlement_amount FLOAT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS invoice_line (
  id SERIAL PRIMARY KEY,
  invoice_id INT NOT NULL REFERENCES invoice(id),
  line_id INT NOT NULL REFERENCES line(id)
);

CREATE TABLE IF NOT EXISTS purchases (
  id SERIAL PRIMARY KEY,
  company_id INT NOT NULL REFERENCES company(id),
  num_doc TEXT DEFAULT NULL,
  mod_pag TEXT DEFAULT NULL,
  total_merc FLOAT DEFAULT NULL,
  total_iva FLOAT DEFAULT NULL,
  total_desc FLOAT DEFAULT NULL,
  num_contribuinte TEXT DEFAULT NULL,
  nome TEXT DEFAULT NULL,
  documento TEXT DEFAULT NULL,
  total_documento FLOAT DEFAULT NULL,
  line_number TEXT DEFAULT NULL,
  product_code INT NOT NULL REFERENCES product(id),
  quantidade INT DEFAULT NULL,
  unit_price FLOAT DEFAULT NULL,
  date_v TEXT DEFAULT NULL
);
