CREATE OR REPLACE VIEW get_all_companies AS
  SELECT s.id, s.name, s.path_saft, s.path_purchases, c.id as company_id
  FROM company c
    INNER JOIN saft_file s ON s.id = c.saft_id;

CREATE OR REPLACE VIEW get_customers AS
  SELECT
  c.customer_id,
  c.company_id,
  c.company_name,
  c.account_id,
  c.tax_id,
  c.b_address,
  c.city,
  c.postal_code,
  c.region,
  a.g_category,
  a.o_credit,
  a.o_debit,
  a.c_credit,
  a.c_debit
  FROM account a
    INNER JOIN customer c ON c.account_id = a.id;

CREATE OR REPLACE VIEW get_suppliers AS
  SELECT
  s.supplier_id,
  s.company_id,
  s.company_name,
  s.account_id,
  s.tax_id,
  s.b_address,
  s.city,
  s.postal_code,
  s.region,
  a.g_category,
  a.o_credit,
  a.o_debit,
  a.c_credit,
  a.c_debit
  FROM account a
    INNER JOIN supplier s ON s.account_id = a.id;

CREATE OR REPLACE VIEW get_invoices AS
  SELECT
  i.company_id,
  i.status,
  i.billing,
  i.period as invoice_period,
  i.i_type,
  i.tax_payable,
  i.net_total,
  i.gross_total,
  i.withholding_amount,
  l.l_number,
  l.quantity,
  l.unit,
  l.unit_price,
  l.credit_amount,
  l.tax_percentage,
  l.settlement_amount,
  t.period as transaction_period,
  t.t_date,
  t.description,
  t.t_type,
  c.company_name as customer_name,
  c.account_id as customer_account,
  p.p_description,
  p.p_code,
  p.p_group,
  p.p_type
  FROM invoice i
    INNER JOIN invoice_line il ON i.id = il.invoice_id
    INNER JOIN line l ON il.line_id = l.id
    INNER JOIN product p ON l.product_id = p.id
    INNER JOIN transaction t ON i.transaction_id = t.id
    INNER JOIN customer c ON i.customer_id = c.id;


CREATE OR REPLACE VIEW get_purchases AS
  SELECT
	p.id,
	p.company_id,
	p.num_doc,
	p.mod_pag,
	p.total_merc,
	p.total_iva,
	p.total_desc,
	p.num_contribuinte,
	p.nome,
	p.documento,
	p.total_documento,
	p.line_number,
	p.product_code,
	p.quantidade,
	p.unit_price,
	p.date_v,
	pd.p_group as product_gname,
	pd.p_description as product_description
  FROM purchases p
    INNER JOIN product pd ON p.product_code = pd.id;