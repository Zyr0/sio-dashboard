-- saft_add
-- saft_delete
-- saft_path

-- Add saft file
CREATE OR REPLACE FUNCTION saft_add(
  _name TEXT,
  _path_saft TEXT,
  _path_purchases TEXT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _id INT;
BEGIN
  INSERT INTO saft_file(name, path_saft, path_purchases) VALUES(_name, _path_saft, _path_purchases);
  SELECT id INTO _id FROM saft_file ORDER BY id DESC LIMIT 1;
  RETURN _id;
END; $$;

-- Delete saft file
CREATE OR REPLACE FUNCTION saft_delete(
  _id INT
)
RETURNS FILES
LANGUAGE plpgsql
AS $$
DECLARE
  _path FILES;
  _path_saft TEXT;
  _path_purchases TEXT;
BEGIN
  SELECT path_saft INTO _path_saft FROM saft_file WHERE id = _id;
  SELECT path_purchases INTO _path_purchases FROM saft_file WHERE id = _id;
  DELETE FROM saft_file WHERE saft_file.id = _id;
  _path.saft = _path_saft;
  _path.purchases = _path_purchases;
  RETURN _path;

  IF NOT FOUND THEN
    RAISE EXCEPTION 'Error deleting saft with id %.', _id;
  END IF;
END; $$;

-- Get saft file info
CREATE OR REPLACE FUNCTION saft_path (
  _id INT
)
RETURNS FILES
LANGUAGE plpgsql
AS $$
DECLARE
  _path FILES;
  _path_saft TEXT;
  _path_purchases TEXT;
BEGIN
  SELECT path_saft INTO _path_saft FROM saft_file WHERE id = _id;
  SELECT path_purchases INTO _path_purchases FROM saft_file WHERE id = _id;
  _path.saft = _path_saft;
  _path.purchases = _path_purchases;
  RETURN _path;

  IF NOT FOUND THEN
    RAISE EXCEPTION 'No saft with id %.', _id;
  END IF;
END; $$;

-- Get Saft Info
CREATE OR REPLACE FUNCTION saft_info(
  IN _id INT
)
RETURNS SETOF saft_file
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY SELECT s.* FROM saft_file s WHERE s.id = _id;

  IF NOT FOUND THEN
    RAISE EXCEPTION 'No saft file fwith id %.', _id;
  END IF;
  RETURN;
END; $$;
