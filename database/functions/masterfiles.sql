-- account_add
-- customer_add
-- supplier_add
-- product_add

-- Add an account
CREATE OR REPLACE FUNCTION account_add(
  _company_id INT,
  _account_id TEXT,
  _description TEXT,
  _g_category TEXT,
  _g_code TEXT,
  _o_debit FLOAT,
  _o_credit FLOAT,
  _c_debit FLOAT,
  _c_credit FLOAT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _id INT;
BEGIN
  INSERT INTO account(
    company_id,
    account_id,
    description,
    g_category,
    g_code,
    o_debit,
    o_credit,
    c_debit,
    c_credit)
  VALUES (
    _company_id,
    _account_id,
    _description,
    _g_category,
    _g_code,
    _o_debit,
    _o_credit,
    _c_debit,
    _c_credit);

  SELECT id INTO _id FROM account ORDER BY id DESC LIMIT 1;
  RETURN _id;
END; $$;

-- Add a customer
CREATE OR REPLACE FUNCTION customer_add (
  _company_id INT,
  _customer_id TEXT,
  _account_id TEXT,
  _tax_id INT,
  _company_name TEXT,
  _b_address TEXT,
  _city TEXT,
  _postal_code TEXT,
  _region TEXT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _v_account_id INT;
  _id INT;
BEGIN
  SELECT id INTO _v_account_id FROM account WHERE account_id = _account_id AND company_id = _company_id;

  INSERT INTO customer(
    company_id,
    customer_id,
    account_id,
    tax_id,
    company_name,
    b_address,
    city,
    postal_code,
    region
    )
  VALUES(
    _company_id,
    _customer_id,
    _v_account_id,
    _tax_id,
    _company_name,
    _b_address,
    _city,
    _postal_code,
    _region
  );
  SELECT id INTO _id FROM customer ORDER BY id DESC LIMIT 1;
  RETURN _id;
END; $$;

-- Add a supplier
CREATE OR REPLACE FUNCTION supplier_add (
  _company_id INT,
  _supplier_id TEXT,
  _account_id TEXT,
  _tax_id INT,
  _company_name TEXT,
  _b_address TEXT,
  _city TEXT,
  _postal_code TEXT,
  _region TEXT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _v_account_id INT;
  _id INT;
BEGIN
  SELECT id INTO _v_account_id FROM account WHERE account_id = _account_id AND company_id = _company_id;

  INSERT INTO supplier(
    company_id,
    supplier_id,
    account_id,
    tax_id,
    company_name,
    b_address,
    city,
    postal_code,
    region)
  VALUES(
    _company_id,
    _supplier_id,
    _v_account_id,
    _tax_id,
    _company_name,
    _b_address,
    _city,
    _postal_code,
    _region
  );
  SELECT id INTO _id FROM supplier ORDER BY id DESC LIMIT 1;
  RETURN _id;
END; $$;

-- Add a product
CREATE OR REPLACE FUNCTION product_add (
  _company_id INT,
  _type TEXT,
  _code TEXT,
  _group TEXT,
  _description TEXT,
  _number_code TEXT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _v_account_id INT;
  _id INT;
BEGIN
  INSERT INTO product(
    company_id,
    p_type,
    p_code,
    p_group,
    p_description,
    p_number_code)
  VALUES(
    _company_id,
    _type,
    _code,
    _group,
    _description,
    _number_code
  );
  SELECT id INTO _id FROM product ORDER BY id DESC LIMIT 1;
  RETURN _id;
END; $$;
