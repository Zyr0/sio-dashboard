-- purchases add

-- Add purchases
CREATE OR REPLACE FUNCTION purchases_add (
  _company_id INT,
  _num_doc TEXT,
  _mod_pag TEXT,
  _total_merc FLOAT,
  _total_iva FLOAT,
  _total_desc FLOAT,
  _num_contribuinte TEXT,
  _nome TEXT,
  _documento TEXT,
  _total_documento FLOAT,
  _line_number TEXT,
  _product_code TEXT,
  _quantidade INT,
  _unit_price FLOAT,
  _date_v TEXT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _v_product_id INT;
  _id INT;
BEGIN
  SELECT id INTO _v_product_id FROM product WHERE p_code = _product_code AND company_id = _company_id;

  INSERT INTO purchases (
    company_id,
    num_doc,
    mod_pag,
    total_merc,
    total_iva,
    total_desc,
    num_contribuinte,
    nome,
    documento,
    total_documento,
    line_number,
    product_code,
    quantidade,
    unit_price,
    date_v)
  VALUES (
    _company_id,
    _num_doc,
    _mod_pag,
    _total_merc,
    _total_iva,
    _total_desc,
    _num_contribuinte,
    _nome,
    _documento,
    _total_documento,
    _line_number,
    _v_product_id,
    _quantidade,
    _unit_price,
    _date_v);
  SELECT id INTO _id FROM purchases ORDER BY id DESC LIMIT 1;
  RETURN _id;
END; $$;
