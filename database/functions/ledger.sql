-- journal_add
-- private -> transactions_journal_add
-- transaction_add
-- private -> transactions_credit_debit_line_add
-- credit_debit_line_add

-- Add journal
CREATE OR REPLACE FUNCTION journal_add (
  _company_id INT,
  _journal_id TEXT,
  _description TEXT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _id INT;
BEGIN
  INSERT INTO journal (
    company_id,
    journal_id,
    description)
  VALUES (
    _company_id,
    _journal_id,
    _description);

  SELECT id INTO _id FROM journal ORDER BY id DESC LIMIT 1;
  RETURN _id;
END; $$;

-- Add transaction to jounal
CREATE OR REPLACE FUNCTION  transaction_journal_add (
  _journal_id INT,
  _transaction_id INT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _id INT;
BEGIN
  INSERT INTO transaction_journal (
    journal_id,
    transaction_id)
  VALUES (
    _journal_id,
    _transaction_id);

  SELECT id INTO _id FROM transaction_journal ORDER BY id DESC LIMIT 1;
  RETURN _id;
END; $$;

-- Add transaction
CREATE OR REPLACE FUNCTION transaction_add (
  _journal_id INT,
  _company_id INT,
  _transaction_id TEXT,
  _period TEXT,
  _t_date TEXT,
  _description TEXT,
  _t_type TEXT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _id INT;
BEGIN
  INSERT INTO transaction (
    company_id,
    transaction_id,
    period,
    t_date,
    description,
    t_type)
  VALUES (
    _company_id,
    _transaction_id,
    _period,
    _t_date,
    _description,
    _t_type);

  SELECT id INTO _id FROM transaction ORDER BY id DESC LIMIT 1;
  PERFORM transaction_journal_add(_journal_id, _id);
  RETURN _id;
END; $$;

-- Add credit_debit_line to transaction
CREATE OR REPLACE FUNCTION transaction_credit_debit_line_add (
  _credit_debit_line_id INT,
  _transaction_id INT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _id INT;
BEGIN
  INSERT INTO transaction_credit_debit_line (
    credit_debit_line_id,
    transaction_id)
  VALUES (
    _credit_debit_line_id,
    _transaction_id);

  SELECT id INTO _id FROM transaction_credit_debit_line ORDER BY id DESC LIMIT 1;
  RETURN _id;
END; $$;

-- Add credit_debit_line
CREATE OR REPLACE FUNCTION credit_debit_line_add (
  _transaction_id INT,
  _company_id INT,
  _l_type TEXT,
  _record_id TEXT,
  _account_id TEXT,
  _source_document TEXT,
  _description TEXT,
  _amount FLOAT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _v_account_id INT;
  _id INT;
BEGIN
  SELECT id INTO _v_account_id FROM account WHERE account_id = _account_id AND company_id = _company_id;

  INSERT INTO credit_debit_line(
    company_id,
    l_type,
    record_id,
    account_id,
    source_document,
    description,
    amount)
  VALUES (
    _company_id,
    _l_type,
    _record_id,
    _v_account_id,
    _source_document,
    _description,
    _amount);

  SELECT id INTO _id FROM credit_debit_line ORDER BY id DESC LIMIT 1;
  PERFORM transaction_credit_debit_line_add(_id, _transaction_id);
  RETURN _id;
END; $$;

