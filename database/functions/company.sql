-- address_info
-- company_info
-- company_add

-- Get Address Info
CREATE OR REPLACE FUNCTION address_info(
  IN _id INT
)
RETURNS SETOF address
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY SELECT a.* FROM address a WHERE a.id = _id;

  IF NOT FOUND THEN
    RAISE EXCEPTION 'No address with id %.', _id;
  END IF;
  RETURN;
END; $$;

-- Get Company Info
CREATE OR REPLACE FUNCTION company_info(
  IN _id INT
)
RETURNS SETOF company
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY SELECT c.* FROM company c WHERE c.id = _id;

  IF NOT FOUND THEN
    RAISE EXCEPTION 'No company with id %.', _id;
  END IF;
  RETURN;
END; $$;

-- Add company
CREATE OR REPLACE FUNCTION company_add(
  _saft_id INT,
  _company_id INT,
  _company_name TEXT,
  _tax_registration_number INT,
  _tax_accounting_basis TEXT,
  _tax_entity TEXT,
  _fiscal_year INT,
  _start_date TEXT,
  _end_date TEXT,
  _created_date TEXT,
  _currency_code VARCHAR(3),
  _product_company_tax_id INT,
  _telephone INT,
  _fax INT,
  _email TEXT,
  _street TEXT,
  _detail TEXT,
  _city TEXT,
  _postal_code TEXT,
  _country TEXT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _address_id INT;
  _id INT;
BEGIN
  INSERT INTO address(street, detail, city, postal_code, country) VALUES(_street, _detail, _city, _postal_code, _country);
  SELECT id INTO _address_id FROM address ORDER BY id DESC LIMIT 1;
  INSERT INTO company(
    saft_id,
    company_id,
    company_name,
    tax_registration_number,
    tax_accounting_basis,
    tax_entity,
    fiscal_year,
    start_date,
    end_date,
    created_date,
    currency_code,
    product_company_tax_id,
    telephone,
    fax,
    email,
    address_id)
  VALUES (
    _saft_id,
    _company_id,
    _company_name,
    _tax_registration_number,
    _tax_accounting_basis,
    _tax_entity,
    _fiscal_year,
    _start_date,
    _end_date,
    _created_date,
    _currency_code,
    _product_company_tax_id,
    _telephone,
    _fax,
    _email,
    _address_id);
  SELECT id INTO _id FROM company ORDER BY id DESC LIMIT 1;
  RETURN _id;
END; $$;
