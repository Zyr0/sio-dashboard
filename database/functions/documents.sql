-- invoice_add
-- private -> line_add
-- invoice_line_add

-- Add invoice
CREATE OR REPLACE FUNCTION invoice_add (
  _company_id INT,
  _invoice_no TEXT,
  _atcud TEXT,
  _status TEXT,
  _billing TEXT,
  _period TEXT,
  _i_type TEXT,
  _customer_id TEXT,
  _transaction_id TEXT,
  _tax_payable FLOAT,
  _net_total FLOAT,
  _gross_total FLOAT,
  _withholding_amount FLOAT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _v_customer_id INT;
  _v_transaction_id INT;
  _id INT;
BEGIN
  SELECT id INTO _v_customer_id FROM customer WHERE customer_id = _customer_id AND company_id = _company_id;
  SELECT id INTO _v_transaction_id FROM transaction WHERE transaction_id = _transaction_id AND company_id = _company_id;

  INSERT INTO invoice (
    company_id,
    invoice_no,
    atcud,
    status,
    billing,
    period,
    i_type,
    customer_id,
    transaction_id,
    tax_payable,
    net_total,
    gross_total,
    withholding_amount)
  VALUES (
    _company_id,
    _invoice_no,
    _atcud,
    _status,
    _billing,
    _period,
    _i_type,
    _v_customer_id,
    _v_transaction_id,
    _tax_payable,
    _net_total,
    _gross_total,
    _withholding_amount);

  SELECT id INTO _id FROM invoice ORDER BY id DESC LIMIT 1;
  RETURN _id;
END; $$;

-- Add line
CREATE OR REPLACE FUNCTION line_add (
  _company_id INT,
  _l_number INT,
  _product_id TEXT,
  _quantity INT,
  _unit TEXT,
  _unit_price FLOAT,
  _credit_amount FLOAT,
  _tax_percentage INT,
  _settlement_amount FLOAT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _v_product_id INT;
  _id INT;
BEGIN
  SELECT id INTO _v_product_id FROM product WHERE p_code = _product_id AND company_id = _company_id;

  INSERT INTO line (
    company_id,
    l_number,
    product_id,
    quantity,
    unit,
    unit_price,
    credit_amount,
    tax_percentage,
    settlement_amount)
  VALUES (
    _company_id,
    _l_number,
    _v_product_id,
    _quantity,
    _unit,
    _unit_price,
    _credit_amount,
    _tax_percentage,
    _settlement_amount);

  SELECT id INTO _id FROM line ORDER BY id DESC LIMIT 1;
  RETURN _id;
END; $$;

-- Add invoice line
CREATE OR REPLACE FUNCTION invoice_line_add (
  _company_id INT,
  _invoice_id INT,
  _l_number INT,
  _product_id TEXT,
  _quantity INT,
  _unit TEXT,
  _unit_price FLOAT,
  _credit_amount FLOAT,
  _tax_percentage INT,
  _settlement_amount FLOAT
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
  _id INT;
BEGIN
  SELECT line_add(
    _company_id,
    _l_number,
    _product_id,
    _quantity,
    _unit,
    _unit_price,
    _credit_amount,
    _tax_percentage,
    _settlement_amount)
  INTO _id;

  INSERT INTO invoice_line (
    invoice_id,
    line_id)
  VALUES (
    _invoice_id,
    _id);

  RETURN _id;
END; $$;

