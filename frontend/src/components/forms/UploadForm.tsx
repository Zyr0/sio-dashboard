import React from 'react';

import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

const name_helper = "The name of the file (will apear on the menu)";

interface IProps {
  values: (values: any) => void;
}

function UploadForm(props: IProps) {
  const [values, setValues] = React.useState({ name: '', files: new FormData() });

  React.useEffect(() => {
    props.values(values);
  });

  const handleChangeFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file_list = e.target.files;
    if (!file_list) return;

    values.files.append(e.target.name, file_list[0], file_list[0].name);

    setValues(prev => {
      return { ...prev, files: values.files }
    })
  }

  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setValues(prev => {
      return { ...prev, name: e.target.value }
    })
  }

  return (
    <React.Fragment>
      <Grid container spacing={2} direction="row" justify="flex-start" alignItems="center">
        <Grid item xs={12} sm={6}>
          <TextField
          required
          aria-required
          margin="normal"
          label="Name"
          type="text"
          value={values.name}
          helperText={name_helper}
          onChange={handleNameChange}
          fullWidth/>
        </Grid>
        <Grid item xs={12} sm={6}>
          <input required aria-required accept=".xml" id="saft-upload" name="saft" type="file" onChange={handleChangeFile} />
        </Grid>
      </Grid>

      <Grid container spacing={2} direction="row" justify="flex-start" alignItems="center">
        <Grid item xs={12} sm={6}>
          <label htmlFor="purchases-upload">
            <Typography variant="body1" component="span"> Purchases csv file: </Typography>
          </label>
        </Grid>
        <Grid item xs={12} sm={6}>
          <input required aria-required accept=".csv" id="purchases-upload" name="purchases" type="file" onChange={handleChangeFile} />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default UploadForm;
