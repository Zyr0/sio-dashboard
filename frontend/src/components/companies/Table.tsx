import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';

interface IProps {
  info: any;
  list: any;
}

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    marginBottom: theme.spacing(2)
  },
  container: {
    maxHeight: 540
  },
}));

function TableInfo(props: IProps) {
  const { list } = props;

  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (e: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const header_products = () => {
    return (
      <TableRow>
        <TableCell>Description</TableCell>
        <TableCell>Code</TableCell>
        <TableCell>Group</TableCell>
      </TableRow>
    )
  };

  const body_products = (row: any) => {
    return (
      <TableRow key={row.p_description}>
        <TableCell>{row.p_description}</TableCell>
        <TableCell>{row.p_code}</TableCell>
        <TableCell>{row.p_group}</TableCell>
      </TableRow>
    )
  };

  const header_others = () => {
    return (
      <TableRow>
        <TableCell>Name</TableCell>
        <TableCell>Account</TableCell>
        <TableCell>Tax</TableCell>
        <TableCell>Address</TableCell>
        <TableCell>City</TableCell>
        <TableCell>Postal Code</TableCell>
        <TableCell>Region</TableCell>
      </TableRow>
    )
  };

  const body_others = (row: any) => {
    return (
      <TableRow key={row.company_name}>
        <TableCell>{row.company_name}</TableCell>
        <TableCell>{row.account_id}</TableCell>
        <TableCell>{row.tax_id}</TableCell>
        <TableCell>{row.b_address}</TableCell>
        <TableCell>{row.city}</TableCell>
        <TableCell>{row.postal_code}</TableCell>
        <TableCell>{row.region}</TableCell>
      </TableRow>
    )
  };

  return (
    <React.Fragment>
      <Grid container spacing={3} className={classes.root}>
        <Grid item sm={12}>
          <TableContainer className={classes.container}>
            <Table stickyHeader>
              <TableHead>
                {props.info === "Products" ? header_products() : header_others() }
              </TableHead>
              <TableBody>
                {list
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row: any) => props.info === "Products" ? body_products(row) : body_others(row) )}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination rowsPerPageOptions={[10, 25, 100]} component="div" count={list.length} rowsPerPage={rowsPerPage} page={page} onChangePage={handleChangePage} onChangeRowsPerPage={handleChangeRowsPerPage} />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default TableInfo;
