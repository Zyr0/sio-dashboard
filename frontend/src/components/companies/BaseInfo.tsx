import React from 'react';
import Typography from '@material-ui/core/Typography';
import Table from './Table';
import { createStyles, Grid, makeStyles, Paper, Theme } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserFriends, faTruckMoving, faBoxes } from '@fortawesome/free-solid-svg-icons';

const styles = makeStyles((theme: Theme) => createStyles({
  paper: {
    width: '100%',
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
  paper_title: {
    width: '100%',
    padding: theme.spacing(2),
    color: theme.palette.text.primary
  },
  title:{
    marginBottom: theme.spacing(2)
  }
}));

interface IProps {
  company: any;
  customers: any;
  suppliers: any;
  products: any;
}

function BaseInfo(props: IProps) {
  const { company } = props;

  const classes = styles();

  const print_values = (name: string, value: string) => {
    return (
      <Typography variant="body1" component="p">
        <b>{name}: </b> {value ? value : 'No entry'}
      </Typography>
    );
  }

  return (
    <React.Fragment>
      <Grid container spacing={3} justify="center">
        <Grid item sm={12} md={8}>
          <Paper className={classes.paper_title} square elevation={1}>
            <Grid container direction="column" justify="center" alignItems="flex-start" item spacing={1}>
              <Typography variant="h4" component="h3">{company.company_name}</Typography>
              {print_values('Nif', company.tax_registration_number)}
              {print_values('Currency', company.currency_code)}
              {print_values('Fiscal Year', company.fiscal_year)}
              {print_values('Telephone', company.telephone)}
              {print_values('Email', company.email)}
              {print_values('Fax', company.fax)}
            </Grid>
          </Paper>
        </Grid>
        <Grid container item sm={12} md={8}>
          <Paper square className={classes.paper_title}>
            <Grid container spacing={3} justify="center">
              <Grid item sm={12} md={12}>
                <Typography variant="h5" align="left" className={classes.title} > <FontAwesomeIcon icon={faUserFriends} /> Customers </Typography>
                <Table list={props.customers} info="Customers" />
              </Grid>
              <Grid item sm={12} md={12}>
                <Typography variant="h5" align="left" className={classes.title} > <FontAwesomeIcon icon={faTruckMoving} /> Suppliers </Typography>
                <Table list={props.suppliers} info="Suppliers" />
              </Grid>
              <Grid item sm={12} md={12}>
              <Typography variant="h5" align="left" className={classes.title} > <FontAwesomeIcon icon={faBoxes} /> Products </Typography>
                <Table list={props.products} info="Products" />
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default BaseInfo;
