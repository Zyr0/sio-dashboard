import React from 'react';

import { Link } from 'react-router-dom';
import { useSnackbar, VariantType } from 'notistack';
import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';

import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Tooltip from '@material-ui/core/Tooltip';
import Container from '@material-ui/core/Container';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CircularProgress from '@material-ui/core/CircularProgress';

import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import InfoIcon from '@material-ui/icons/Info';
import GetAppIcon from '@material-ui/icons/GetApp';

import UploadForm from '../forms/UploadForm';
import AlertModal from '../modal/AlertModal';

import company_service from '../../services/company.service';
import upload_service from '../../services/upload.service';

const styles = makeStyles((theme: Theme) => createStyles({
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  del: {
    color: theme.palette.error.main
  },
  info: {
    color: theme.palette.info.main
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  root_grid: {
    flexGrow: 1,
    minHeight: '90vh'
  },
  card: {
    minHeight: '30rem'
  },
  buttons: {
    display: 'flex',
    paddingRight: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    flexDirection: 'row-reverse'
  },
  spacer: {
    marginBottom: theme.spacing(4)
  }
}));

interface IData {
  values: any;
}

interface IDel {
  open: boolean;
  r: any;
}

function ListCompanies() {
  const { enqueueSnackbar } = useSnackbar();
  const [loading, _setLoading] = React.useState(true);
  const [failed, _setFailed] = React.useState(false);
  const [open, _setOpen] = React.useState(false);
  const [del, _setDel] = React.useState<IDel>({ open: false, r: {} });
  const [formValues, _setFormValues] = React.useState({ name: '', files: new FormData() });
  const [data, _setData] = React.useState<IData>({ values: {} });
  const classes = styles();

  const file = React.useCallback(() => {
    _setLoading(true);
    company_service.get_all()
      .then(d => {
        const values = { count: d.count, rows: d.rows };
        _setData({ values });
        _setFailed(false);
        _setLoading(false);
      }).catch(err => {
        enqueueSnackbar(`${err.message ? err.message : err}`, { variant: 'error' });
        _setLoading(false);
        _setFailed(true);
      });
  }, [enqueueSnackbar]);

  React.useEffect(() => {
    file();
  }, [file]);

  const getValues = (values: any) => {
    _setFormValues(values);
  }

  const updateStatus = (message: string, variant: VariantType) => {
    enqueueSnackbar(message, { variant });
    _setLoading(false);
    file();
  }

  const handleConfirm = (type: string) => () => {
    const newValues = { ...del, open: false };
    _setDel(newValues);
    if (type === 'NO') return;

    _setLoading(true);
    upload_service.delete_file(del.r.id)
      .then(data => updateStatus(data.message, 'success'))
      .catch(err => updateStatus(`${err.message ? err.message : err}`, 'error'));
  }

  const handleUpload = (type: string) => () => {
    _setLoading(true);
    _setOpen(false);
    if (type === 'NO') return _setLoading(false);

    if (!formValues.name) {
      enqueueSnackbar("Missing name", { variant: 'error' });
      _setLoading(false);
      return
    }

    enqueueSnackbar("Uploading File!", { variant: 'info' });
    upload_service.upload_file(formValues)
      .then(d => updateStatus(`${d.originalname} saved`, 'success'))
      .catch(err => updateStatus(`${err.message ? err.message : err}`, 'error'));
  }

  const handleDeleteButton = (r: any) => () => {
    const values = { open: true, r };
    _setDel(values);
  }

  const handleDownloadButton = (id: number, csv: boolean) => {
    return upload_service.download_file_url(id, csv);
  }

  const card = (r: any) => (
    <Grid item key={r.id} xs={12} sm={6} md={4}>
      <Card raised>
        <CardContent>
          <Typography variant='h5' component='h2' gutterBottom>{r.name}</Typography>
          <Typography variant='body2' component="p" color='textSecondary'>{r.path_saft}</Typography>
          <Typography variant='body2' component="p" color='textSecondary'>{r.path_purchases}</Typography>
        </CardContent>
        <CardActions className={classes.buttons}>
          <Link to={`/company/${r.company_id}`}>
            <Tooltip title={`See info about ${r.name}`}>
              <IconButton className={classes.info} aria-label={`See info about ${r.name}`}>
                <InfoIcon />
              </IconButton>
            </Tooltip>
          </Link>
          <Tooltip title={`Delete ${r.name}`}>
            <IconButton onClick={handleDeleteButton(r)} className={classes.del} aria-label={`Delete ${r.name}`}>
              <DeleteForeverIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title={`Download ${r.path_saft}`}>
            <IconButton href={handleDownloadButton(r.id, false)} aria-label={`Download ${r.path_saft}`}>
              <GetAppIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title={`Download ${r.path_purchases}`}>
            <IconButton href={handleDownloadButton(r.id, true)} aria-label={`Download ${r.path_purchases}`}>
              <GetAppIcon />
            </IconButton>
          </Tooltip>
        </CardActions>
      </Card>
    </Grid>
  );

  const full_valid = () => { return !loading && !failed }

  return (
    <Container>
      <AlertModal
        open={open}
        fullWidth
        maxWidth='md'
        title={'Upload SAF-T PT File'}
        desc={'Upload the Standard Audit File for Tax purposes file in xml format.'}
        values={getValues}
        Component={UploadForm}
        handleClose={handleUpload} />

      <AlertModal
        open={del.open}
        fullWidth
        maxWidth='sm'
        title={`Are you sure whant to delete ${del.r.name}?`}
        desc="This action is permanent."
        handleClose={handleConfirm} />

      <Grid container direction="row" justify="center" alignItems="center" spacing={4} className={classes.root_grid}>
        {loading && <Grid item> <CircularProgress size="4rem" /> </Grid>}
        {failed && <Typography variant="h2">Cannot get information</Typography>}
        {full_valid() && data.values.count > 0 && data.values.rows.map(card)}
        {full_valid() && data.values.count <= 0 && <Typography variant="h2">No SAF-T's Uploaded</Typography>}
      </Grid>

      <Fab onClick={() => _setOpen(true)}
        disabled={loading}
        variant="extended"
        color="secondary"
        aria-label="Upload SAFT"
        className={classes.fab}>
        <CloudUploadIcon className={classes.extendedIcon} fontSize="large" />
      Upload
    </Fab>
    </Container>
  );
}



export default ListCompanies;
