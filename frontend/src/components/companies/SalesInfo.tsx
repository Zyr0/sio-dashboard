import React from 'react';

import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import PieChart from '../charts/PieChart';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoins , faDollarSign, faChartLine, faChartPie } from '@fortawesome/free-solid-svg-icons';

import Chart from '../charts/Chart';

const months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
const styles = makeStyles((theme: Theme) => createStyles({
  paper: {
    width: '100%',
    padding: theme.spacing(2)
  },
  spacer: {
    marginTop: theme.spacing(4)
  },
  title:{
    marginBottom: theme.spacing(2)
  }
}));

interface IProps {
  currency: any,
  sales_info: any,
  purchases_info: any
}

const setupCustomers = (sales_info: any) => {
  const customers_value = sales_info.reduce((acc: any, obj: any) => {
    let key = obj.customer_name
    if (!acc[key]) {
      acc[key] = []
    }
    acc[key].push(obj)
    return acc
  },{});

  let customers: any = [];
  for (let c in customers_value)
    customers.push({ name: c, credit_amount: customers_value[c].reduce((acc: any, obj: any) => acc + obj.credit_amount, 0)});
  customers.sort((a: any, b: any) => a.credit_amount - b.credit_amount);
  return customers.reverse();
}

const setupProducts = (sales_info: any) => {
  const products_v = sales_info.reduce((acc: any, obj: any) => {
    let key = obj.p_description
    if (!acc[key]) {
      acc[key] = []
    }
    acc[key].push(obj)
    return acc
  },{});

  let products: any = [];
  for(let p in products_v)
    products.push({ name: p , v: products_v[p].reduce((acc: any, obj: any) => acc + obj.credit_amount, 0) });
  products.sort((a: any, b: any) => a.v - b.v);
  return products.reverse();
}

const gross_margin = (net_sales: any, cost: any) =>{
  return (((net_sales - cost) / net_sales) * 100).toFixed(3);
}

const profit = (net_sales: any, cost: any) => {
  return (net_sales - cost) 
}

const setup_pur = (purchases: any) => {
  let purchases_p: any = [];
  purchases.forEach((p: any) => {
    purchases_p.push({
      date: p['date_v'].split("-")[1] ,
      value: (parseInt(p.unit_price)*parseInt(p.quantidade))
    })
  })
  return purchases_p;
}

const get_purchases_data = (purchases: any) => {
  const net_data = purchases.reduce((acc: any, obj: any) => {
    if (obj.date in acc)
      acc[obj.date] += obj.value;
    else
      acc[obj.date] = obj.value;
    return acc
  },[]);
  const ordered_data = Object.keys(net_data).sort().reduce(
    (obj: any, key: any) => { 
      obj[key] = net_data[key]; 
      return obj;
    },{}
  );
  return ordered_data
}

const get_profit_month = (sales_month: any, purchases_month: any) => {
  let data: any = [];
  let i =0;
  sales_month.forEach((s: any) => {
    let profit = s - parseInt(purchases_month[i])
    data.push(profit)
    i++;
  })
  return data;
}

function SalesInfo(props: IProps) {
 
  const classes = styles();
  const { currency, sales_info, purchases_info } = props;

  const total_net_price = sales_info.reduce((acc: any, obj: any) => acc + parseInt(obj.credit_amount), 0);
  const total_gross = (total_net_price as number) * 1.23;
  const total_cost = purchases_info.reduce((acc: any, obj: any) => acc + (parseInt(obj.unit_price)*parseInt(obj.quantidade)), 0);

  const credit_amount_categories = sales_info.reduce((acc: any, obj: any) => {
    let index = parseInt(obj.invoice_period) - 1;
    if (acc.indexOf(months[index]) === -1) acc[index] = months[index];
    return acc
  }, []).filter((v: any) => v);

  const credit_amount_data = sales_info.reduce((acc: any, obj: any) => {
    if (obj.invoice_period in acc) acc[obj.invoice_period] += obj.credit_amount;
    else acc[obj.invoice_period] = obj.credit_amount;
    return acc
  }, []).filter((v: any) => v);

  const customers = setupCustomers(sales_info);
  const products = setupProducts(sales_info);

  let pie_labels_products: any = []
  let pie_data_products: any = []
  products.forEach((p: any) => {
    let arr1 = Object.values(p);
    pie_labels_products.push(arr1[0])
    pie_data_products.push(arr1[1])
  })

  let pie_labels_customers: any = []
  let pie_data_customers: any = [] 
  customers.forEach((p: any) => {
    let arr1 = Object.values(p);
    pie_labels_customers.push(arr1[0])
    pie_data_customers.push(arr1[1])
  })

  const to_currency = (value: any) => {
    return Number(value).toLocaleString(navigator.language, { style: 'currency', currency })
  }
   
  const net_purchases_values = Object.values(get_purchases_data(setup_pur(purchases_info)))
  const profit_month = get_profit_month(credit_amount_data, net_purchases_values)
  console.log(profit_month)

  return (
    <React.Fragment>
      <Grid container spacing={4} justify= "center">
        <Grid container item xs={12} sm={3}>
          <Paper square className={classes.paper}>
            <Typography variant="h5" align="left"> <FontAwesomeIcon icon={faCoins} /> Sales Gross Total</Typography>
            <Typography variant="h3" align="center">{to_currency(total_gross)}</Typography>
          </Paper>
        </Grid>
        <Grid container item xs={12} sm={3}>
          <Paper square className={classes.paper}>
            <Typography variant="h5" align="left"> <FontAwesomeIcon icon={faCoins} /> Sales Net Total </Typography>
            <Typography variant="h3" align="center"> {to_currency(total_net_price)} </Typography>
          </Paper>
        </Grid>
        <Grid container item xs={12} sm={2}>
          <Paper square className={classes.paper}>
            <Typography variant="h5" align="left"> <FontAwesomeIcon icon={faDollarSign} /> Gross margin</Typography>
            <Typography variant="h3" align="center" >{gross_margin(total_net_price, total_cost)}%</Typography>
          </Paper>
        </Grid>
        <Grid container item xs={12} sm={2}>
          <Paper square className={classes.paper}>
            <Typography variant="h5" align="left"> <FontAwesomeIcon icon={faDollarSign} /> Profit</Typography>
            <Typography variant="h3" align="center" >{to_currency(profit(total_net_price, total_cost))}</Typography>
          </Paper>
        </Grid>
      </Grid>
      <Grid container spacing={4} justify="center" >
        <Grid container item xs={10} sm={8}>
          <Paper square className={classes.paper} elevation={2}>
            <Typography variant="h5" align="center">
            <FontAwesomeIcon icon={faChartLine} /> Net total of sales per month
          </Typography>
            <Chart
            type="line"
            series={[ { data: credit_amount_data } ]}
            xaxis={{ categories: credit_amount_categories }} />
          </Paper>
        </Grid>
      </Grid>
      <Grid container spacing={3} justify="center" >
        <Grid container item xs={5}>
          <Paper square className={classes.paper} elevation={2}>
          <Typography variant="h5" align="center">
            <FontAwesomeIcon icon={faChartPie} /> Top 10 Customers
          </Typography>
            <PieChart title= "" data={pie_data_customers.splice(0, 10)} labels={pie_labels_customers.splice(0, 10)}/>
          </Paper>
        </Grid>
        <Grid container item xs={5}>
          <Paper square className={classes.paper} elevation={2}>
          <Typography variant="h5" align="center">
            <FontAwesomeIcon icon={faChartPie} /> Top 10 Products
          </Typography>
            <PieChart title= "" data={pie_data_products.splice(0, 10)} labels={pie_labels_products.splice(0, 10)}/>
          </Paper>
        </Grid>
      </Grid> 
      <Grid container spacing={4} justify="center" >
        <Grid container item xs={10} sm={8}>
          <Paper square className={classes.paper} elevation={2}>
            <Typography variant="h5" align="center">
            <FontAwesomeIcon icon={faChartLine} /> Profit per month
          </Typography>
            <Chart
            type="bar"
            series={[ { data: profit_month } ]}
            xaxis={{ categories: credit_amount_categories }} />
          </Paper>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default SalesInfo;
