import React from 'react';

import { useSnackbar } from 'notistack';
import { RouteComponentProps } from "react-router-dom";
import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';

import BusinessIcon from '@material-ui/icons/Business';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';

import BaseInfo from './BaseInfo';
import SalesInfo from './SalesInfo';

import company_service from '../../services/company.service';
import PurchasesInfo from './PurchasesInfo';

const styles = makeStyles((theme: Theme) => createStyles({
  card: {
    padding: theme.spacing(2),
    margin: theme.spacing(2),
  },
  loading_grid: {
    flexGrow: 1,
    minHeight: '90vh'
  },
  root: {
    backgroundColor: theme.palette.background.default,
    width: '100%',
    minHeight: '100vh'
  },
  tabs: {
    backgroundColor: "#D05353",
    color: theme.palette.background.paper
  },
  panel: {
    width: '100%',
    padding: theme.spacing(2)
  }
}));

type Tparams = { id: string };
interface IData { values: any };
interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const classes = styles();
  const { children, value, index, ...other } = props;

  return (
    <div
    role="tabpanel"
    hidden={value !== index}
    id={`info-panel-${index}`}
    aria-labelledby={`info-panel-${index}`}
    className={classes.panel}
    {...other}>
    {value === index && (children)}
  </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `info-panel-${index}`,
    'aria-controls': `info-panel-${index}`,
  };
}

function InfoCompany({ match }: RouteComponentProps<Tparams>) {
  const { enqueueSnackbar } = useSnackbar();
  const [failed, _setFailed] = React.useState(false);
  const [loading, _setLoading] = React.useState(false);
  const [data, _setData] = React.useState<IData>({ values: {} });
  const [value, setValue] = React.useState(0);
  const classes = styles();

  const info = React.useCallback(() => {
    _setLoading(true);
    _setFailed(false);
    company_service.get_info(match.params.id)
    .then(r => {
      _setData({ values: r.data });
      _setFailed(false);
      _setLoading(false);
    })
    .catch(err => {
      enqueueSnackbar(err, { variant: 'error' });
      _setFailed(true);
      _setLoading(false);
    })
  }, [match, enqueueSnackbar]);

  console.log(data.values);

  React.useEffect(() => {
    info();
  }, [info]);

  const handleChange = (e: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };
   
  const panels = (
    <React.Fragment>
      <TabPanel value={value} index={0}>
        {data.values.invoices && <SalesInfo currency={data.values.company_info.currency_code} sales_info={data.values.invoices} 
          purchases_info={data.values.purchases} />}
      </TabPanel>
      <TabPanel value={value} index={1}>
         <PurchasesInfo purchases_info={data.values.purchases}  />  
      </TabPanel>
      <TabPanel value={value} index={2}>
        {data.values.company_info &&
          <BaseInfo company={data.values.company_info} customers={data.values.customers} suppliers={data.values.suppliers} products={data.values.products} />
        }
      </TabPanel>
    </React.Fragment>
  );

  const tabs = (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs centered value={value} onChange={handleChange} className={classes.tabs} aria-label="Info tabs">
          <Tab label="Sales" {...a11yProps(1)} icon={<TrendingUpIcon />} />
          <Tab label="Purchases" {...a11yProps(2)} icon={<ShowChartIcon />} />
          <Tab label="Company" {...a11yProps(0)} icon={<BusinessIcon />} />
        </Tabs>
      </AppBar>
      {data.values && panels}
    </div>
  );

  return (
    <React.Fragment>
      {(loading || failed) && (
        <Grid container alignItems="center" justify="center" className={classes.loading_grid}>
          {loading ? <CircularProgress size="4rem" /> : <Typography variant="h2"> Cannot get information </Typography>}</Grid>)}
          {!failed && !loading && data.values && tabs}
          </React.Fragment>);
}

export default InfoCompany;
