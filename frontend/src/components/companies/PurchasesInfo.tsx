import React from 'react';

import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

import Chart from '../charts/Chart';
import { Grid, Typography } from '@material-ui/core';
import { Paper } from '@material-ui/core';
import { faCoins , faChartLine, faChartPie } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PieChart from '../charts/PieChart';

const months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
const styles = makeStyles((theme: Theme) => createStyles({
  paper: {
    width: '100%',
    padding: theme.spacing(2)
  },
  spacer: {
    marginTop: theme.spacing(4)
  },
  title:{
    marginBottom: theme.spacing(2)
  }
}));

interface IProps { purchases_info: any }

const setupSuppliers = (purchases: any) => {
    const suppliers_value = purchases.reduce((acc: any, obj: any) => {
      let key = obj.nome
      if (!acc[key]) {
        acc[key] = []
      }
      acc[key].push(obj)
      return acc;
    },{});
  
    let supliers: any = [];
    for (let s in suppliers_value)
      supliers.push({ name: s, total_merc: suppliers_value[s].reduce((acc: any, obj: any) => acc + (parseInt(obj.unit_price)*parseInt(obj.quantidade)), 0)});
  
    supliers.sort((a: any, b: any) => a.total_merc - b.total_merc);
    return supliers;
  }

const setupTopPurchases = (purchases: any) => {
  const purchases_v = purchases.reduce((acc: any, obj: any) => {
    let key = obj.product_description
    if (!acc[key]) {
      acc[key] = []
    }
    acc[key].push(obj)
    return acc;
  },{});

  let purchases_prod: any = [];
  for (let p in purchases_v)
    purchases_prod.push({ name: p, v: purchases_v[p].reduce((acc: any, obj: any) => acc + (parseInt(obj.unit_price)*parseInt(obj.quantidade)), 0)});
  purchases_prod.sort((a: any, b: any) => b.v - a.v);
  console.log("HIII",purchases_prod);
  return purchases_prod;
}

const setup_pur = (purchases: any) => {
  let purchases_p: any = [];
  purchases.forEach((p: any) => {
    purchases_p.push({
      date: p['date_v'].split("-")[1] ,
      value: (parseInt(p.unit_price)*parseInt(p.quantidade))
    })
  })
  return purchases_p;
}

const get_purchases_data = (purchases: any) => {
  const net_data = purchases.reduce((acc: any, obj: any) => {
    if (obj.date in acc)
      acc[obj.date] += obj.value;
    else
      acc[obj.date] = obj.value;
    return acc
  },[]);
  const ordered_data = Object.keys(net_data).sort().reduce(
    (obj: any, key: any) => { 
      obj[key] = net_data[key]; 
      return obj;
    },{}
  );
  return ordered_data
}

const categories = (purchases: any) => {
  const cat = purchases.reduce((acc: any, obj: any) => {
    let index = parseInt(obj.date) - 1;
    if (acc.indexOf(months[index]) === -1) acc[index] = months[index];
    return acc
  }, []).filter((v: any) => v); 
  return cat
}

function PurchasesInfo(props: IProps) {
    const classes = styles();
    const { purchases_info} = props;
    const suppliers = setupSuppliers(purchases_info);
    const purchased_products = setupTopPurchases(purchases_info);
    const total_cost = purchases_info.reduce((acc: any, obj: any) => acc + (parseInt(obj.unit_price)*parseInt(obj.quantidade)), 0);

    const to_currency = (value: any) => {  
      let n = Math.abs(value);
      return Number(n).toLocaleString(navigator.language, { style: 'currency', currency: 'EUR' })  
    }

    const purchases = setup_pur(purchases_info);

    let bar_labels: any = []
    let bar_data: any = [] 
    suppliers.forEach((p: any) => {
      let arr1 = Object.values(p);
      bar_labels.push(arr1[0])
      bar_data.push(arr1[1])
    })

    let pie_labels: any = []
    let pie_data: any = [] 
    purchased_products.forEach((p: any) => {
      let arr1 = Object.values(p);
      pie_labels.push(arr1[0])
      pie_data.push(arr1[1])
    })
    
    return (
        <React.Fragment>
          <Grid container spacing={3} justify="center" >
            <Grid container item xs={12} sm={3}>
            <Paper square className={classes.paper}>
              <Typography variant="h5" align="left"> <FontAwesomeIcon icon={faCoins} /> Total Net Purchases</Typography>
              <Typography variant="h3" align="center">{to_currency(total_cost)}</Typography>
            </Paper>
            </Grid>
          </Grid>
          <Grid container spacing={4} justify="center" >
            <Grid container item xs={10} sm={8}>
              <Paper square className={classes.paper} elevation={2}>
                <Typography variant="h5" align="center">
                <FontAwesomeIcon icon={faChartLine} /> Net total purchases per month
              </Typography>
                <Chart
                type="line"
                series={[ { data: Object.values(get_purchases_data(purchases)) } ]}
                xaxis={{ categories: categories(purchases) }} />
              </Paper>
            </Grid>
          </Grid>
          <Grid container spacing={4} justify="center" >
          <Grid container item xs={10} sm={8}>
            <Paper square className={classes.paper} elevation={2}>
              <Typography variant="h5" align="center">
              <FontAwesomeIcon icon={faChartLine} /> Net total purchases per supplier
            </Typography>
              <Chart
              type="bar"
              series={[ { data: bar_data } ]}
              xaxis={{ categories: bar_labels }} />
            </Paper>
          </Grid>
          </Grid>
          <Grid container spacing={4} justify="center" >
            <Grid container item xs={5}>
            <Paper square className={classes.paper} elevation={2}>
              <Typography variant="h5" align="center">
                <FontAwesomeIcon icon={faChartPie} /> Top 10 Purchased Products
              </Typography>
                <PieChart title= "" data={pie_data.splice(0, 10)} labels={pie_labels.splice(0, 10)}/>
            </Paper>
          </Grid>  
          </Grid>
        </React.Fragment>
        )
}

export default PurchasesInfo;