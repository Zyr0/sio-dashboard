import React from 'react';

import { Route, Switch } from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Navbar from './navigation/Navbar';
import ListCompanies from './companies/ListCompanies';
import InfoCompany from './companies/InfoCompany';

function NotFound() {
  return (
    <Grid container alignItems="center" justify="center" style={{ flexGrow: 1, height: '90vh' }}>
      <Typography variant="h2"> Page not found! </Typography>
    </Grid>
  );
}

function App() {
  return (
    <React.Fragment>
      <Navbar />
      <Switch>
        <Route exact path="/" component={ListCompanies} />
        <Route path="/company/:id" component={InfoCompany} />
        <Route path="*" component={NotFound} />
      </Switch>
    </React.Fragment>
  );
}

export default App;
