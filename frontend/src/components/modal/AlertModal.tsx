import React from 'react';

import withMobileDialog from '@material-ui/core/withMobileDialog';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

interface IState {}
interface IProps {
  open: boolean;
  title: string;
  maxWidth: 'xs' | 'sm' | 'md' | 'lg' | 'xl';
  handleClose: (type: string) => any;
  values?: {};
  Component?: React.ComponentType<any>;
  desc?: string;
  fullScreen?: boolean;
  fullWidth?: boolean;
}

class AlertModal extends React.Component<IProps, IState> {
  render() {
    const { open, fullScreen, fullWidth, maxWidth, title, desc, values, Component, handleClose } = this.props;

    return (
      <Dialog
      open={open}
      fullScreen={fullScreen ? fullScreen : false}
      onClose={handleClose('NO')}
      fullWidth={fullWidth ? fullWidth : false}
      maxWidth={maxWidth}
      aria-labelledby="dialog-title">
      <DialogTitle id="dialog-title">{title}</DialogTitle>
      <DialogContent>
        {desc && <DialogContentText style={{ whiteSpace: 'pre-wrap' }}>{desc}</DialogContentText>}
        {Component && <Component values={values} />}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose('NO')} color="primary">
          CANCEL
        </Button>
        <Button onClick={handleClose('OK')} color="primary">
          OK
        </Button>
      </DialogActions>
    </Dialog>
    );
  }
}

export default withMobileDialog()(AlertModal);
