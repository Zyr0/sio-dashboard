import React from 'react';
import ReactApexChart from 'react-apexcharts';

interface IProps{
  data: any,
  labels: any,
  title: any,
  height?: any
}

interface IState {
  series: any,
  options: any
};

class PieChart extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      series: props.data,
      options: {
        colors: ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', 
        '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', 
        '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', 
        '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', 
        '#808080', '#ffffff', '#000000'],
        chart: { type: 'pie', with: 'auto' },
        labels: props.labels,
        title: {
          text: props.title,
          align: 'center'
        }
      },
    };
  }

  render() {
    return (
      <ReactApexChart
      options={this.state.options}
      series={this.state.series}
      type="pie" height={this.props.height ? this.props.height : '400px'}
    />);
  }
}

export default PieChart
