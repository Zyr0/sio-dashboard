import React from 'react';
import ReactApexChart from 'react-apexcharts';

interface IProps {
  title?: String,
  type: 'bar' | 'line',
  series: any,
  xaxis: any,
  height?: any,
  horizontal?: boolean,
  lables?: boolean
};

interface IState {
  series: any,
  options: any
};

class Chart extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      series: props.series,
      options: {
        chart: { type: props.type, with: 'auto' },
        plotOptions: {
          bar: { borderRadius: 3, horizontal: props.horizontal }
        },
        dataLabels: { enabled: props.lables },
        xaxis: props.xaxis,
        title: {
          text: props.title ? props.title : '',
          align: 'center'
        }
      },
    };
  }

  render() {
    return (
      <ReactApexChart
      options={this.state.options}
      series={this.state.series}
      type={this.props.type}
      height={this.props.height ? this.props.height : '400px'}/>
    );
  }
}

export default Chart;
