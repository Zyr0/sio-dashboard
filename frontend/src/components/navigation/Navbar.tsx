import React from 'react';

import { Link } from "react-router-dom";
import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = makeStyles((theme: Theme) => createStyles({
  appbar: {
    background: theme.palette.secondary.main
    // background: `linear-gradient(45deg, ${theme.palette.secondary.main} 30%, ${theme.palette.primary.main} 90%)`
  },
  ul: {
    listStyle: 'none',
    margin: 0,
    padding: 0,
    overflow: 'hidden'
  },
  li: {
    float: 'left'
  },
  navbar_brand: {
    display: 'block',
    padding: '14px 16px',
    textDecoration: 'none',
    color: theme.palette.secondary.contrastText
  }
}));

export default function Navbar() {
  const classes = styles();

  return (
    <React.Fragment>
      <AppBar className={classes.appbar}>
        <Toolbar>
          <ul className={classes.ul}>
            <li className={classes.li}> <Link to='/' className={classes.navbar_brand}> <Typography variant="h6">SAF-T PT</Typography> </Link> </li>
          </ul>
        </Toolbar>
      </AppBar>
      <Toolbar />
    </React.Fragment>
  );
}
