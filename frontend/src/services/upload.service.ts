import configs from '../helpers/configs';

const upload_file = (data: { name: string, files: FormData  }) => {
  const request_options = {
    method: 'POST',
    body: data.files
  };

  return fetch(`${configs.backend}/upload/${data.name}`, request_options)
    .then(configs.handleResponse)
    .then(data => data.file);
}

const delete_file = (id: number) => {
  const request_options = {
    method: 'DELETE'
  };

  return fetch(`${configs.backend}/upload/${id}`, request_options)
    .then(configs.handleResponse)
    .then(data => data);
}

const download_file_url = (id: number, csv: boolean) => {
  return `${configs.backend}/upload/${id}/${csv}`;
}

const upload_service = {
  upload_file,
  delete_file,
  download_file_url
};

export default upload_service;
