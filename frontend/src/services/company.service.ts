import configs from '../helpers/configs';

const get_all = () => {
  return fetch(`${configs.backend}/company`, configs.get_method)
    .then(configs.handleResponse)
    .then(data => data);
}

const get_info = (id: any) => {
  return fetch(`${configs.backend}/company/${id}`, configs.get_method)
    .then(configs.handleResponse)
    .then(data => data);
}

const company_service = {
  get_all,
  get_info,
};

export default company_service;
