const handleResponse = (res: any) => {
  return res.text().then((text: any) => {
    const data = text && JSON.parse(text);
    if (!res.ok) {
      const error = (data && data.error) || res.statusText;
      return Promise.reject(error);
    }
    return data;
  });
}

const get_method = {
  method: 'GET'
}

const configs = {
  backend: 'http://localhost:8080/api',
  handleResponse,
  get_method
};

export default configs;
