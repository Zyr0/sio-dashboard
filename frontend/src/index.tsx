import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider, createMuiTheme, makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { SnackbarProvider } from 'notistack';
import App from './components/App';
const shape = require('./shape.svg').default;

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#4e16c9"
    },
    secondary: {
      main: "#191919"
    },
  }
});

const styles = makeStyles((theme: Theme) => createStyles({
  root: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    overflowY: 'auto',
    backgroundColor: theme.palette.grey["100"],
    background: `url(${shape}) no-repeat`,
    backgroundSize: 'cover',
    backgroundAttachment: 'fixed'
  },
}));

function Jsx() {
  const classes = styles();

  return (
    <React.Fragment>
      <CssBaseline />
      <div className={classes.root}>
        <ThemeProvider theme={theme}>
          <SnackbarProvider anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }} maxSnack={3}>
            <BrowserRouter>
              <App />
            </BrowserRouter>
          </SnackbarProvider>
        </ThemeProvider>
      </div>
    </React.Fragment>
  );
}

ReactDOM.render(<React.StrictMode> <Jsx /> </React.StrictMode>, document.getElementById('root'));
