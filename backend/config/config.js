require('dotenv').config();

let CONFIG = {};

CONFIG.app = process.env.APP || 'dev';
CONFIG.port = process.env.PORT || '8080';

let db = {};

db.user = process.env.DB_USER || 'postgres';
db.host = process.env.DB_HOST || 'localhost';
db.database = process.env.DB_DATABASE || 'testdb';
db.password = process.env.DB_PASSWORD || 'testdb';
db.port = process.env.DB_PORT || 5432;

CONFIG.db = db;

module.exports = CONFIG;
