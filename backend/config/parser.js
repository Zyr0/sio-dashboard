const fs = require('fs/promises');
const fs_n = require('fs');
const xml2js = require('xml2js');
const xml2js_libs = require('xml2js/lib/processors.js');
const csv = require("fast-csv");

const company_model = require('../models/company');

function isIterable(obj) {
  return Array.isArray(obj);
}

/**
 * Parse the xml file
 * @param path the path of the xml file to parse
 * @return a promiss
 */
const parse = (path) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, "utf-8")
      .then(data => data)
      .then(data => xml2js.parseStringPromise(data, {
        valueProcessors: [xml2js_libs.parseNumbers],
        explicitArray: false
      }))
      .then(r => resolve(r.AuditFile))
      .catch(err => reject(err.message));
  });
};

/**
 * Parse the csv file
 * @param company_id the id of the company
 * @param path the path of the csv file to parse
 * @return a promiss
 */
const parse_csv = (company_id, path) => {
  return new Promise(async (resolve, reject) => {
    let val = [];
    csv.parseFile(path, { headres: true, delimiter: ';' })
      .on("error", error => reject(error.message))
      .on("data", data => {
        const v = {
          NumDoc: data[0],
          ModoPag: data[1],
          TotalMerc: Number.parseFloat(data[2]),
          TotalIva: Number.parseFloat(data[3]),
          TotalDesc: Number.parseFloat(data[4]),
          NumContribuinte: data[5],
          Nome: data[6],
          Documento: data[7],
          TotalDocumento: Number.parseFloat(data[8]),
          LineNumber: data[9],
          ProductCode: data[10],
          Quantidade: Number.parseInt(data[11]),
          UnitPrice: Number.parseFloat(data[12]),
          Date: data[13]
        };
        val.push(v);
      })
      .on("end", () => {
        val.forEach(v => {
          let err;
          company_model.add_purchases(company_id, v).catch(e => err = e.message);
          if (err) return reject(err);
        });
      })
    resolve("Ok");
  });
};

/**
 * Handle master files in xml to the database
 */
const master_files = async (company_id, result) => {
  const accounts = result['GeneralLedgerAccounts']['Account'];
  const customers = result['Customer'];
  const suppliers = result['Supplier'];
  const products = result['Product'];

  return new Promise(async (resolve, reject) => {
    if (accounts)
      for (account of accounts) await company_model.add_account(company_id, account).catch(e => reject(e.message));
    if (customers)
      for (customer of customers) await company_model.add_customer(company_id, customer).catch(e => reject(e.message));
    if (suppliers)
      for (supplier of suppliers) await company_model.add_supplier(company_id, supplier).catch(e => reject(e.message));
    if (products)
      for (product of products) await company_model.add_product(company_id, product).catch(e => reject(e.message));
    resolve('Ok');
  })
};

/**
 * handle ledger entries
 */
const ledger_entries = async (company_id, result) => {
  const journals = result['Journal'];

  return new Promise(async (resolve, reject) => {
    let err;
    for (journal of journals) {
      let journal_id;
      await company_model.add_journal(company_id, journal)
        .then(data => journal_id = data.rows[0].journal_add)
        .catch(e => err = e.message);
      if (err) { reject(err); return; }

      const transactions = journal['Transaction'];
      if (isIterable(transactions)) {
        for (transaction of transactions) {
          err = await handle_transaction(journal_id, company_id, transaction)
          if (err) { reject(err); return; }
        };
      } else {
        if (transactions)
          err = await handle_transaction(journal_id, company_id, transactions);
      }

      if (err) { reject(err); return; }
    }
    resolve('Ok');
  })
};

/**
 * handle the transaction
 */
const handle_transaction = async (journal_id, company_id, transaction) => {
  let transaction_id
  let err;
  await company_model.add_transaction(journal_id, company_id, transaction)
    .then(data => transaction_id = data.rows[0].transaction_add)
    .catch(e => err = e.message);
  if (err) { return err; }

  const debits = transaction['Lines']['DebitLine'];
  err = handle_transaction_line(transaction_id, company_id, 'd', debits);
  if (err) { return err; }

  const credits = transaction['Lines']['CreditLine'];
  handle_transaction_line(transaction_id, company_id, 'c', credits);
  if (err) { return err; }
}

/**
 * handle the transaction line
 */
const handle_transaction_line = async (transaction_id, company_id, type, lines) => {
  let err;
  if (isIterable(lines))
    for (line of lines) {
      await company_model.add_credit_debit_line(transaction_id, company_id, type, line).catch(e => err = e.message);
      if (err) { return err; }
    }
  else
    await company_model.add_credit_debit_line(transaction_id, company_id, type, lines).catch(e => err = e.message);
  return err;
}

/**
 * handle the documents
 */
const documents = async (company_id, result) => {
  const invoices = result['SalesInvoices']['Invoice'];

  return new Promise(async (resolve, reject) => {
    let err;

    if (!invoices) return reject("No invoices");

    for (invoice of invoices) {
      let invoice_id
      await company_model.add_invoice(company_id, invoice)
        .then(data => invoice_id = data.rows[0].invoice_add)
        .catch(e => err = e.message);
      if (err) { reject(err); return; };

      const lines = invoice['Line'];
      if (isIterable(lines)) {
        for (line of lines) {
          await company_model.add_invoice_line(company_id, invoice_id, line).catch(e => err = e.message);
          if (err) { reject(err); return; };
        }
      } else {
        await company_model.add_invoice_line(company_id, invoice_id, lines).catch(e => err = e.message);
        if (err) { reject(err); return; };
      }
    }
    if (err) { reject(err); return; }
    resolve('Ok');
  })
};


const parser = {
  parse,
  master_files,
  documents,
  ledger_entries,
  parse_csv
};

module.exports = parser;
