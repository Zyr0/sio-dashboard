const { Pool } = require('pg');
const CONFIG = require('./config');

const pool = new Pool({
  user: CONFIG.db.user,
  host: CONFIG.db.host,
  database: CONFIG.db.database,
  password: CONFIG.db.password,
  port: CONFIG.db.port,
});

module.exports = {
  query: (text, params) => pool.query(text, params),
};
