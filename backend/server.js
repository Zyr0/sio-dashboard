const express = require('express');
const logger = require('morgan');
const app = express();
const fs = require('fs');
const path = require('path');
const cors = require('cors');
const body_parser = require('body-parser');
const multer = require('multer');

const CONFIG = require('./config/config');

app.use(logger('dev'));
app.use(cors());
app.use(body_parser.urlencoded({ extended: false }))
app.use(body_parser.json());

const routePath = path.join(__dirname, 'routes');
fs.readdirSync(routePath).forEach(e => {
  const file = require(path.join(routePath, e));
  app.use(`/api/${file.name}`, file.router);
});

/**
 * Handle internal erros.
 */
app.use((err, req, res, next) => {
  console.log(err);
  res.status(err.status || 500).send({ error: err.message ? err.message : 'somthing broke!' });
});

/**
 * Handle 404 erros.
 */
app.use((req, res, next) => {
  res.status(404).send({ error: 'Nothing to see here!!' });
});

app.listen(CONFIG.port);
console.log('App listening on: ', CONFIG.port);
