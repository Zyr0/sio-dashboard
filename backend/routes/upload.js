const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const multer = require('multer');

const parser = require('../config/parser');
const upload_model = require('../models/upload');
const company_model = require('../models/company');

const upload_path = path.join(__dirname, '../', 'uploads/');

const storage = multer.diskStorage({
  destination: (req, file, cb) => cb(null, upload_path),
  filename: (req, file, cb) => {
    fs.access(upload_path.concat(file.originalname), fs.constants.F_OK, (err) => {
      if (err) return cb(null, file.originalname);
      cb(new Error("File already exists"));
    })
}});

const upload = multer({ storage });
const fields = upload.fields([ { name: 'saft', maxCount: 1 }, { name: 'purchases', maxCount: 1 } ])

/**
 * Post file addes file to database and parses file and adds info to database
 * Returns the filename
 * Returns a 500 if there is a database problem or a file problem
 */
router.post('/:name', fields, async (req, res) => {
  if (!req.files || !req.files['saft'] || !req.files['purchases'])
    return res.status(400).send({ error: "Missing file" });

  const saft = req.files['saft'][0];
  const purchases = req.files['purchases'][0];
  const { name } = req.params;

  // Add file to database
  let saft_id;
  let has_err;
  await upload_model.add({ name, saft: saft.filename, purchases: purchases.filename })
    .then(data => saft_id = data.rows[0].saft_add)
    .catch(err => {
      has_err = err;
      fs.unlinkSync(saft.path); // remove file if error adding to database
      fs.unlinkSync(purchases.path); // remove file if error adding to database
    });
  if (has_err) return res.status(500).send({ error: has_err.message });

  // Parse file to add to database
  let parser_result;
  let company_id;
  const saft_path = path.join(upload_path, saft.filename);
  await parser.parse(saft_path)
    .then(result => parser_result = result)
    .then(() => company_model.add_company(saft_id, parser_result['Header']))
    .then(r => company_id = r.rows[0].company_add)
    .then(() => parser.master_files(company_id, parser_result['MasterFiles']))
    .then(() => parser.ledger_entries(company_id, parser_result['GeneralLedgerEntries']))
    .then(() => parser.documents(company_id, parser_result['SourceDocuments']))
    .then(() => parser.parse_csv(company_id, path.join(upload_path, purchases.filename)))
    .then(() => res.status(200).send( { file: saft, company_id: company_id }))
    .catch(async e => {
      if (company_id)
        await company_model.delete_company_by_saft(saft_id);
      // delete from database if file has errors
      await upload_model.delete(saft_id)
        .catch(err => e = err.message);
      fs.unlinkSync(saft.path, ((err) => err ? e = err.message : e = err )); // remove file if error parsing file
      fs.unlinkSync(purchases.path, ((err) => err ? e = err.message : e = err )); // remove file if error parsing file
      res.status(500).send({ error: e.message ? e.message : e });
    });
});

/**
 * Delete file from the database and uploads folder.
 * Returns a message saing ok
 * Returns a 500 if there is a database problem or a file problem
 */
router.delete('/:id', async (req, res) => {
  const { id } = req.params;
  if (!Number.parseInt(id, 10)) return res.status(400).send({ error: 'Param id is not a number' });

  await company_model.delete_company_by_saft(id)
    .then(async () => {
      await upload_model.delete(id)
        .then(async r => {
          const saft = r.rows[0].saft;
          const purchases = r.rows[0].purchases
          if (!saft || !purchases) return res.status(500).send({ message: 'File not in database' });

          let error = '';
          fs.unlinkSync(path.join(upload_path, saft), ((err) =>  err ? error = err.message : error = err)); // remove file if error parsing file
          fs.unlinkSync(path.join(upload_path, purchases), ((err) =>  err ? error = err.message : error = err)); // remove file if error parsing file
          if (error !== '') { return res.status(500).send({ error }) }

          res.status(200).send({ message: 'Deleted successfully' });

        }).catch(err => res.status(500).send({ error: err.message ? err.message : err }));
    }).catch(err => res.status(500).send({ error: err.message ? err.message : err }));
});

/**
 * Download file
 * Returns a file if it exists
 * Error if not or if database fails
 */
router.get('/:id/:csv', async (req, res) => {
  const { id, csv } = req.params;
  if (!Number.parseInt(id, 10)) return res.status(400).send({ error: 'Param id is not a number' });

  await upload_model.get_path(id)
    .then(r => {
      const val = r.rows[0];
      const file = csv === 'true' ? val.purchases : val.saft;
      if (file === null) return res.status(500).send({ error: 'File does not exists' })
      res.download(path.join(upload_path, file));
    })
    .catch(err => res.status(500).send({ error: err.message ? err.message : err }));
});

module.exports = { name: 'upload', router };
