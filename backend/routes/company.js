const express = require('express');
const router = express.Router();

const company_model = require('../models/company');

/**
 * Get all of the companies id, saft_id, saft_name and saft_file
 */
router.get('/', async (_, res) => {
  await company_model.get_all()
  .then(data => res.status(200).send({ rows: data.rows, count: data.rowCount }))
  .catch(err => res.status(500).send({ error: err.message ? err.message : "Database error" }))
});

/**
 * get all info from company
 */
router.get('/:id', async (req, res) => {
  const { id } = req.params;
  await company_model.get_by_id(id)
    .then(data => res.status(200).send({ data }))
    .catch(err => res.status(500).send({ error: err.message ? err.message : "Database error" }));
});

module.exports = { name: 'company', router };
