const db = require('../config/database');

const get_all = () => {
  return db.query("SELECT * FROM get_all_companies", []);
};

const get_by_id = async (id) => {
  let company_info;
  let saft_info;
  let address_info;
  let customers;
  let suppliers;
  let products;
  let invoices;
  let purchases;
  return db.query("SELECT * FROM company_info($1)", [id])
    .then(data => company_info = data.rows[0])
    .then(() => db.query("SELECT * FROM saft_info($1)", [company_info.saft_id]))
    .then(data => saft_info = data.rows[0])
    .then(() => db.query("SELECT * FROM address_info($1)", [company_info.address_id]))
    .then(data => address_info = data.rows[0])
    .then(() => db.query("SELECT * FROM get_customers WHERE company_id = $1", [company_info.id]))
    .then(data => customers = data.rows)
    .then(() => db.query("SELECT * FROM get_suppliers WHERE company_id = $1", [company_info.id]))
    .then(data => suppliers = data.rows)
    .then(() => db.query("SELECT * FROM product WHERE company_id = $1", [company_info.id]))
    .then(data => products = data.rows)
    .then(() => db.query("SELECT * FROM get_invoices WHERE company_id = $1", [company_info.id]))
    .then(data => invoices = data.rows)
    .then(() => db.query("SELECT * FROM get_purchases WHERE company_id = $1", [company_info.id]))
    .then(data => purchases = data.rows)
    .then(() => ({ company_info, saft_info, address_info, customers, suppliers, products, invoices, purchases }));
};

const add_company = (saft_id, header) => {
  return db.query("SELECT company_add($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20)", [
    saft_id,
    header['CompanyID'],
    header['CompanyName'],
    header['TaxRegistrationNumber'],
    header['TaxAccountingBasis'],
    header['TaxEntity'],
    header['FiscalYear'],
    header['StartDate'],
    header['EndDate'],
    header['DateCreated'],
    header['CurrencyCode'],
    header['ProductCompanyTaxID'],
    header['Telephone'],
    header['Fax'],
    header['Email'],
    header['CompanyAddress']['StreetName'],
    header['CompanyAddress']['AddressDetail'],
    header['CompanyAddress']['City'],
    header['CompanyAddress']['PostalCode'],
    header['CompanyAddress']['Country'],
  ]);
};

const add_account = (company_id, account) => {
  return db.query("SELECT account_add($1, $2, $3, $4, $5, $6, $7, $8, $9)", [
    company_id,
    account['AccountID'],
    account['AccountDescription'],
    account['GroupingCategory'],
    account['GroupingCode'],
    account['OpeningDebitBalance'],
    account['OpeningCreditBalance'],
    account['ClosingDebitBalance'],
    account['ClosingCreditBalance'],
  ]);
};

const add_customer = (company_id, customer) => {
  return db.query("SELECT customer_add($1, $2, $3, $4, $5, $6, $7, $8, $9)", [
    company_id,
    customer['CustomerID'],
    customer['AccountID'],
    customer['CustomerTaxID'],
    customer['CompanyName'],
    customer['BillingAddress']['AddressDetail'],
    customer['BillingAddress']['City'],
    customer['BillingAddress']['PostalCode'],
    customer['BillingAddress']['Region']
  ]);
};

const add_supplier = (company_id, supplier) => {
  return db.query("SELECT supplier_add($1, $2, $3, $4, $5, $6, $7, $8, $9)", [
    company_id,
    supplier['SupplierID'],
    supplier['AccountID'],
    supplier['SupplierTaxID'],
    supplier['CompanyName'],
    supplier['BillingAddress']['AddressDetail'],
    supplier['BillingAddress']['City'],
    supplier['BillingAddress']['PostalCode'],
    supplier['BillingAddress']['Region']
  ]);
};

const add_product = (company_id, product) => {
  return db.query("SELECT product_add($1, $2, $3, $4, $5, $6)", [
    company_id,
    product['ProductType'],
    product['ProductCode'],
    product['ProductGroup'],
    product['ProductDescription'],
    product['ProductNumberCode'],
  ]);
};

const add_journal = (company_id, jounal) => {
  return db.query("SELECT journal_add($1, $2, $3)", [
    company_id,
    jounal['JournalID'],
    jounal['Description'],
  ]);
};

const add_transaction = (jounal_id, company_id, transaction) => {
  return db.query("SELECT transaction_add($1, $2, $3, $4, $5, $6, $7)", [
    jounal_id,
    company_id,
    transaction['TransactionID'],
    transaction['Period'],
    transaction['TransactionDate'],
    transaction['Description'],
    transaction['TransactionType'],
  ]);
};

const add_credit_debit_line = async (transaction_id, company_id, type, line) => {
  return await db.query("SELECT credit_debit_line_add($1, $2, $3, $4, $5, $6, $7, $8)", [
    transaction_id,
    company_id,
    type,
    line['RecordID'],
    line['AccountID'],
    line['SourceDocumentID'],
    line['Description'],
    type === 'c' ? line['CreditAmount'] : line['DebitAmount'],
  ]);
}

const add_invoice = async (company_id, invoice) => {
  return await db.query("SELECT invoice_add($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)", [
    company_id,
    invoice['InvoiceNo'],
    invoice['ATCUD'],
    invoice['DocumentStatus']['InvoiceStatus'],
    invoice['DocumentStatus']['SourceBilling'],
    invoice['Period'],
    invoice['InvoiceType'],
    invoice['CustomerID'],
    invoice['TransactionID'],
    invoice['DocumentTotals']['TaxPayable'],
    invoice['DocumentTotals']['NetTotal'],
    invoice['DocumentTotals']['GrossTotal'],
    invoice['WithholdingTax']['WithholdingTaxAmount']
  ]);
};

const add_invoice_line = async (company_id, invoice_id, line) => {
  return await db.query("SELECT invoice_line_add($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)", [
    company_id,
    invoice_id,
    line['LineNumber'],
    line['ProductCode'],
    line['Quantity'],
    line['UnitOfMeasure'],
    line['UnitPrice'],
    line['CreditAmount'],
    line['Tax']['TaxPercentage'],
    line['SettlementAmount'],
  ]);
};

const add_purchases = async (company_id, purchase) => {
  return await db.query("SELECT purchases_add($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)", [
    company_id,
    purchase['NumDoc'],
    purchase['ModPag'],
    purchase['TotalMerc'],
    purchase['TotalIva'],
    purchase['TotalDesc'],
    purchase['NumContribuinte'],
    purchase['Nome'],
    purchase['Documento'],
    purchase['TotalDocumento'],
    purchase['LineNumber'],
    purchase['ProductCode'],
    purchase['Quantidade'],
    purchase['UnitPrice'],
    purchase['Date']
  ]);
};

const delete_company_by_saft = (id) => {
  return db.query("CALL delete_company_by_saft($1)", [id]);
};

const company_model = {
  get_all,
  get_by_id,
  add_company,
  add_account,
  add_customer,
  add_supplier,
  add_product,
  add_journal,
  add_transaction,
  add_credit_debit_line,
  add_invoice,
  add_invoice_line,
  add_purchases,
  delete_company_by_saft
}

module.exports = company_model;
