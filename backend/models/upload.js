const db = require('../config/database');

const add = ({ name, saft, purchases }) => { return db.query('SELECT saft_add($1, $2, $3);', [name, saft, purchases]) };
const _delete = (id) => { return db.query('SELECT * FROM saft_delete($1);', [id]) };
const get_path = (id) => { return db.query('SELECT * FROM saft_path($1);', [id]) };

const upload_model = {
  add,
  delete: _delete,
  get_path
}

module.exports = upload_model;
