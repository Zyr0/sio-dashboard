#!/bin/bash

set -e

read -p "Run -i (drops existing database and creates a new one) option [y|N]: " val
[ "$val" = "Y" ] || [ "$val" = "y" ] && docker exec -it db sh ./create_db.sh -i sio && printf "\n\n"

read -p "Run -d (runs functions procedures and views) option [y|N]: " val
[ "$val" = "Y" ] || [ "$val" = "y" ] && docker exec -it db sh ./create_db.sh -d
